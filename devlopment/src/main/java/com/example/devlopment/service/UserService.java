package com.example.devlopment.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.devlopment.entity.User;
import com.example.devlopment.entity.Error;
import com.example.devlopment.repository.UserRepository;

@Service
public class UserService
{
	@Autowired
	private UserRepository repository;
	
	public List<User> findByNom(String nom)
	{
		List<User> byName = new ArrayList<>();
		List<User> tous = repository.findAll();
		
		for (User user : tous)
	    {
	        if (nom.equals(user.getNom()))
    		{
	        	byName.add(user);
    		}
	    }
			
		return byName;
	}
	
	public List<User> getUsers()
	{
		return repository.findAll();
	}
	
	public Optional<User> getUserById(int id)
	{
		return repository.findById(id);
	}

	public List<User> getUserByName(String name)
	{
		return repository.findByNom(name);
	}
	
	public Error delUserById(int id)
	{
		Error myError = new Error();
		
		try
		{
			repository.deleteById(id);
			myError.setCode(1);
			myError.setMessage("Suppression réalisé");
		}
		catch (org.springframework.dao.EmptyResultDataAccessException e)
		{
			myError.setCode(0);
			myError.setMessage("Y'a une erreur mec : "+e.getMessage());
		}
		
		return myError;
	}
	
	public User addUser(User user)
	{
		return repository.save(user);
	}
	
	public User modUser(int id, User user)
	{
		User toto = repository.getOne(id);
		if (user.getNom() != null)
		{
			toto.setNom(user.getNom());
		}
		
		if (user.getPrenom() != null)
		{
			toto.setPrenom(user.getPrenom());
		}
		
		if(user.getAge() != 0 )
		{
			toto.setAge(user.getAge());
		}
		
		return repository.save(toto);
	}
}
