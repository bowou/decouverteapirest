package com.example.devlopment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevlopmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevlopmentApplication.class, args);
	}

}
