package com.example.devlopment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.devlopment.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>
{
	List<User> findByNom(String nom);
}
