package com.example.devlopment.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.devlopment.entity.User;
import com.example.devlopment.entity.Error;
import com.example.devlopment.service.UserService;

@RestController
public class UserController
{
	@Autowired
	private UserService service;
	
	@GetMapping("/users")
	public List<User> findAllUsers()
	{
		return service.getUsers();
	}
	
	@GetMapping("/usersbyid")
	public Optional<User> findUserByIdBody(@RequestParam int id )
	{
		return service.getUserById(id);
	}
	
	@GetMapping("/usersbyid/{id}")
	public Optional<User> findUserById(@PathVariable int id )
	{
		return service.getUserById(id);
	}
	
	@GetMapping("/usersbyname/{name}")
	public List<User> findUserByName(@PathVariable String name )
	{
		return service.getUserByName(name);
	}
	
	@DeleteMapping("/users/{id}")
	public Error delUserById(@PathVariable int id )
	{		
		return service.delUserById(id);
	}
	
	@PostMapping("/users")
	public User addUser(@RequestBody User user )
	{
		return service.addUser(user);
	}
	
	@PatchMapping("/users/{id}")
	public User modUser(@PathVariable int id, @RequestBody User user )
	{
		return service.modUser(id, user);
	}
}
